<?php

/**
 * Class for working with Sms-Fly service.
 */
class smsFLY {
  const REQUEST_SUCCESS = 'success';
  const REQUEST_ERROR = 'error';

  protected
    $project = NULL,
    $username = NULL,
    $password = NULL,
    $testMode = FALSE,
    $url = 'sms-fly.com/api/api.php',
    $rate = 1,
    $start_time = 'AUTO',
    $end_time = 'AUTO',
    $lifetime = 4,
    $response = NULL;

  /**
   * Constructor.
   *
   * @param string $project
   *   Public function construct string project.
   * @param string $username
   *   Public function construct string username.
   * @param string $password
   *   Public function construct string password.
   * @param string $alfaname
   *   Public function construct string alfaname.
   * @param int $testMode
   *   Public function construct int testmode.
   */
  public function __construct($project, $username, $password, $alfaname = 'InfoCentr', $testMode = FALSE) {
    $this->project = $project;
    $this->username = $username;
    $this->password = $password;
    $this->alfaname = $alfaname;
    $this->testMode = $testMode;
  }

  /**
   * Send SMS.
   *
   * @param string|array $recipients
   *   Public function sendSMS string array recipients.
   * @param string $message
   *   Public function sendSMS string message.
   * @param string $run_at
   *   Public function sendSms string runat.
   *
   * @return bool|int
   *   Public function sendSMS bool int.
   *
   * @deprecated
   */
  public function sendSMS($recipients, $message, $run_at = NULL) {
    return $this->messageSend($recipients, $message, $run_at);
  }

  /**
   * Check message status.
   *
   * @param string|array $messagesId
   *   Public function checkStatus string array messagesId.
   *
   * @return bool|array
   *   Public function checkStatus bool array.
   *
   * @deprecated
   */
  public function checkStatus($messagesId) {
    return $this->messageStatus($messagesId);
  }

  /**
   * Send SMS.
   *
   * @param string|array $recipients
   *   Public function messageSend string array recipients.
   * @param string $message
   *   Public function messageSend string message.
   * @param string $run_at
   *   Public function messageSend string runat.
   *
   * @return bool|int
   *   Public function messageSend bool int.
   */
  public function messageSend($recipients, $message, $run_at = NULL) {
    $params = array(
      'recipients' => $recipients,
      'message' => $message,
    );

    if ($run_at != NULL) {
      $params['run_at'] = $run_at;
    }

    if ($this->testMode) {
      $params['test'] = 1;
    }

    $response = $this->makeRequest('message/send', $params);

    return $response['status'] == self::REQUEST_SUCCESS;
  }

  /**
   * Check message status.
   *
   * @param string|array $messagesId
   *   Public function messageStatus string array messagesId.
   *
   * @return bool|array
   *   Public functin messageStatus bool array.
   */
  public function messageStatus($messagesId) {
    if (!is_array($messagesId)) {
      $messagesId = array($messagesId);
    }

    $response = $this->makeRequest('message/status', array(
      'messages_id' => join(',', $messagesId),
    ));

    return $response['status'] == self::REQUEST_SUCCESS ? $response['messages'] : FALSE;
  }

  /**
   * Get user balance.
   */
  public function userBalance() {
    $response = $this->makeRequest('message/balance');
    return $response['status'] == self::REQUEST_SUCCESS ? $response['balance'] : FALSE;
  }

  /**
   * Get balance.
   */
  public function getBalance() {
    return $this->userBalance();
  }

  /**
   * Send request.
   *
   * @param string $function
   *   Protected function makeRequest string function.
   * @param array $params
   *   Protected function makeRequest array params.
   *
   * @return object
   *   Protected function makeRequest object.
   */
  protected function makeRequest($function, array $params = array()) {
    $url = $this->url;
    $username = $this->username;
    $password = $this->password;
    $alfaname = $this->alfaname;
    $rate = $this->rate;
    $params = $this->joinArrayValues($params);
    $params = array_merge(array('project' => $this->project), $params);
    $ch = curl_init($url);
    $myXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    $myXML .= "<request>";
    $myXML .= "<operation>SENDSMS</operation>";
    $myXML .= '<message start_time="' . $this->start_time . '" end_time="' . $this->end_time . '" lifetime="' . $this->lifetime . '" rate="' . $rate . '" desc="' . $params['project'] . '" source="' . $alfaname . '">' . "\n";
    $myXML .= "<body>" . $params['message'] . "</body>";
    $myXML .= "<recipient>" . $params['recipients'] . "</recipient>";
    $myXML .= "</message>";
    $myXML .= "</request>";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://sms-fly.com/api/api.php');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: text/xml",
      "Accept: text/xml"
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $myXML);
    $response = curl_exec($ch);
    curl_close($ch);
    return $this->response = json_decode($response, TRUE);
  }

  /**
   * Protected functin joinArrayValues.
   */
  protected function joinArrayValues($params) {
    $result = array();
    foreach ($params as $name => $value) {
      $result[$name] = is_array($value) ? join(',', $value) : $value;
    }
    return $result;
  }

  /**
   * Last request.
   *
   * @return array
   *   Public function getResponse array.
   */
  public function getResponse() {
    return $this->response;
  }

}